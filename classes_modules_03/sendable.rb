class Sendable
  @@history = {}
  def self.history
    @@history
  end

  attr_reader :updated_at, :created_at, :sent_at, :from
  attr_accessor :body, :subject, :to

  def initialize(body: nil, subject: nil, from: nil, to: nil)
    @body = body
    @subject = subject
    @from = from
    @to = to
    @created_at = Time.now
    @updated_at = Time.now
    @sent_at = nil
  end

  def send!
    raise DataAlreadySent if @sent_at
    @sent_at = Time.now
    @@history[from] ||= {}
    @@history[from][to] ||= []
    @@history[from][to] << sent_at
    self
  end
end
