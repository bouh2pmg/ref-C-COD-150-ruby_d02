require_relative "email"

class DataAlreadySent < RuntimeError; end
class Private < Email
  def send!
    raise DataAlreadySent if @sent
    @sent = true
    self
  end
end
