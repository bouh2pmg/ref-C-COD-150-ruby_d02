class Sms < Sendable
  def initialize(body: nil, from: nil, to: nil)
    super body: body, from: from, to: to
  end
end
