class SendableBox
  @@all = {}

  def self.recv sendable
    b = @@all[sendable.to]
    b.recv(sendable)
  end

  def initialize contact, max_box, sleep_duration
    @box = []
    @read = []
    @@all[contact] = self
    @t = Thread.new do
      loop do
        sleep sleep_duration
        next if @box.empty?
        @read << @box.pop
        puts "New message from: #{@read.last.from}"
        puts @read.last.subject || @read.last.body
        break if max_box == @read.size
      end
    end
  end

  def recv sendable
    @box << sendable
  end

end
