class DataAlreadySent < RuntimeError; end

class Sendable
  @@history = {}
  def self.history
    @@history
  end
  
  attr_reader :updated_at, :created_at, :sent_at, :from
  attr_accessor :body, :subject, :to
  
  def initialize(body: nil, subject: nil, from: nil, to: nil)
    @body = body
    @subject = subject
    @from = from
    @to = to
    @created_at = Time.now
    @updated_at = Time.now
    @sent_at = nil
  end
  
  def send!
    raise DataAlreadySent if @sent_at
    @sent_at = Time.now
    @@history[from] ||= {}
    @@history[from][to] ||= []
    @@history[from][to] << sent_at
    self
  end
  
  def encrypt! rotate=13
    self.replace Text.new self.each_char.map{|e| ((e.ord+rotate) % 128).chr }.join
    set_secret rotate
    self
  end
  
  def decrypt! rotate=13
    if rotate == :hack
      decrypt! @secret
    else
      self.replace Text.new self.each_char.map{|e| ((e.ord-rotate) % 128).chr }.join
    end
    self
  end
end
