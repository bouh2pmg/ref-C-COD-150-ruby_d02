require_relative "sendable"

class Email < Sendable
  def initialize(body: nil, subject: nil, from: nil, to: nil)
    super body: body, subject: subject, from: from, to: to
  end
end
