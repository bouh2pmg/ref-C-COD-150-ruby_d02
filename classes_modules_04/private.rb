require_relative "email"

class Private < Email
  def send!
    raise DataAlreadySent if @sent
    @sent = true
    self
  end
end
