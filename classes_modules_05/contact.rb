module Addressable
  attr_accessor :address
end
module Nameable
  attr_accessor :name
end
module HasFriend
  def friends
    @friends ||= []
  end
end

class Contact
  include Nameable
  include Addressable
  include HasFriend
end
